const puppeteer = require('puppeteer-extra')

const StealthPlugin = require('puppeteer-extra-plugin-stealth')
puppeteer.use(StealthPlugin())

async function open_tab( url , browser ){
    let  page  = await browser.newPage();
    await page.setViewport({width: 1200, height: 1000});

    let randomUrl = url;

    while (randomUrl) {
        console.log(randomUrl)
        try {
            await page.goto( randomUrl , {timeout:0}).then(async function(response) {
                let hrefs = await page.$$eval('a', as => as.map(a => a.href));
                hrefs.filter(function(href) {
                    return href.includes(url);
                });

                randomUrl = hrefs[Math.floor(Math.random() * hrefs.length)];
                while (!randomUrl.includes(url)) {
                    randomUrl = hrefs[Math.floor(Math.random() * hrefs.length)];
                }
            });
        } catch (err) {
            console.log(err);
            randomUrl = url;
        }

    }
}

(async () => {

    var argv = require('minimist')(process.argv.slice(2));
    const tab = argv.tab ? parseInt(argv.tab) : 50;
    const headless = argv.headless ? argv.headless : true;
    const url = argv.url ? argv.url : 'https://community.vinfastauto.com/';

    const browser = puppeteer.launch({ headless: headless, args: [ '--no-sandbox', '--disable-setuid-sandbox'] }).then(async browser => {
        for (let i = 0; i < tab ; i++)
            open_tab( url, browser);
    });

})()
