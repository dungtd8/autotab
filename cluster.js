const { Cluster } = require('puppeteer-cluster');

const { addExtra } = require('puppeteer-extra');
const puppeteerVanilla = require('puppeteer');

const StealthPlugin = require('puppeteer-extra-plugin-stealth')

const puppeteer = addExtra(puppeteerVanilla);
puppeteer.use(StealthPlugin());

(async () => {
    const cluster = await Cluster.launch({
        puppeteer,
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 2,
        puppeteerOptions: {
            headless: false
        }
    });

    await cluster.task(async ({ page, data: url }) => {
        await page.goto(url);
        const screen = await page.screenshot();
        // Store screenshot, do something else
    });

    // cluster.queue('http://www.google.com/');
    // many more pages

    await cluster.idle();
    await cluster.close();
})();